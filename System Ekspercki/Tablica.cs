﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System_Ekspercki
{
    public class Tablica
    {
        static public string[] tablica = new string[] { 
         "Jedziesz z dziećmi?",
         "Preferuje odpoczynek gdzie jest...",
         "Umiesz pływać?",
         "Czy potrafisz wytrzymać długo w jednym miejscu?",
         "Kiedy planujesz urlop?",
         "Czy uważasz wysiłek fizyczny jako formę wypoczynku?",
         "Masz lęk wysokości?" };

        static public string[] tablicaBox1 = new string[] {
         "Tak",
         "dużo ludzi",
         "Tak",
         "Tak",
         "Wiosna/Lato",
         "Nie",
         "Tak" };

        static public string[] tablicaBox2 = new string[] {
         "Nie",
         "niewiele osób",
         "Nie",
         "Nie",
         "Jesień/Zima",
         "Tak",
         "Nie" };
    }
}
