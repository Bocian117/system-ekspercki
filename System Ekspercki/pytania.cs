﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace System_Ekspercki
{
    public partial class pytania : Form
    {
        int first = 0;
        int second = 0;
        int i = 1;
        public pytania()
        {
            InitializeComponent();
            label1.Text = Tablica.tablica[0];
            radioButton1.Text = Tablica.tablicaBox1[0];
            radioButton2.Text = Tablica.tablicaBox2[0];
            label2.Text = "Pytanie 1";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true && radioButton2.Checked == false)
            {
                first++;
            } else if (radioButton2.Checked == true && radioButton1.Checked == false)
            {
                second++;
            }
            if (Tablica.tablica.Length <= i)
            {
                if (first > second)
                {
                    MessageBox.Show("Preferowane miejsce wypoczynku: Morze");
                }
                else if (first < second)
                {
                    MessageBox.Show("Preferowane miejsce wypoczynku: Góry");
                };

                Application.Exit();
            }
            else
            {

                label1.Text = Tablica.tablica[i];
                radioButton1.Text = Tablica.tablicaBox1[i];
                radioButton2.Text = Tablica.tablicaBox2[i];
                label2.Text = "Pytanie " + ++i;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
